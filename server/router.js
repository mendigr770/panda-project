const express = require('express')
const app = express()
const port = 3000

app.use(express.json())
app.get('/patients/locations/lon/lat/radius', (req, res) => {
  res.send(`/patients/locations/${req.params.lon}/${req.params.lat}/${req.params.radius}`);
});

app.get('/patients/locations/polygon', (req, res) => {
  res.send(req.body);
});

app.get('/patients/cities', (req, res) => {
  res.send(`/patients/locations/cities`);
});

app.get('/patients/between/startDate/endDate', (req, res) => {
  res.send(`/patients/between/${req.params.startDate}/${req.params.endDate}`);
});

app.post('patients/add', (req, res) => {
 res.send(req.body)
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})

